/**
 * Created by sony vaio on 03/07/2015.
 */
var MdlStorage = {
  eventUpdate:null,
  storage:window.localStorage,
  idem:null,
  indexCallback:null,
  isCompatible: function () {
    if (window.sessionStorage && window.localStorage)
    {
      return true;
    } else {
      alert('Lo siento, pero tu navegador no acepta almacenamiento local');
      return false;
    }
  },
  createMsg: function (objMsg) {
    var sObjMsg = {
      from:this.idem,
      msg:objMsg
    };
    return JSON.stringify(sObjMsg);

  },
  sendMsg: function (keyEvent,objMsg) {
    this.storage.setItem(keyEvent, this.createMsg(objMsg));
  },
  eventUpdateStorage: function (event) {
    console.info(event);
    var key = event.key;
    var value = MdlStorage.storage.getItem(key);
    console.info("eventUpdateStorage",key,value);
    value = JSON.parse(value);
    if (MdlStorage.indexCallback) {
      if (MdlStorage.indexCallback[key]) {
        console.info("ecnontrado la accion a realizar");
        MdlStorage.indexCallback[key](value);
      } else {
        console.error("no encontro la accion");
      }
    }
  },

  ini: function () {

    if (this.isCompatible()) {//if(typeof(Storage) !== "undefined")
      window.localStorage.clear();
      this.eventUpdate=  window.addEventListener("storage", MdlStorage.eventUpdateStorage, false);
    }
  }
};

var objEDS = null,marker = null;



/*Date.prototype.yyyymmdd = function() {
 var yyyy = this.getFullYear().toString();
 var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
 var dd  = this.getDate().toString();
 return yyyy +"/"+ (mm[1]?mm:"0"+mm[0]) +"/"+ (dd[1]?dd:"0"+dd[0]); // padding
 };*/

var marker = null;

function loadOption () {
      $.getJSON("data/centrospoblados.min.json",function (data) {
        console.log(data);
        var $centropsearch = $("#centropsearch");

        var departamentos = Object.keys(data);
        for (var i = departamentos.length - 1; i >= 0; i--) {
          var departamento = departamentos[i];
          var municipios = data[departamento];
          for (var j = municipios.length - 1; j >= 0; j--) {
            var municipio = municipios[j];
            countries.push({
              value: departamento+'-'+municipio.n,
              data: municipio.n,
              x:municipio.x,
              y:municipio.y,

            });

            //$centropsearch.append('<option value="'+departamento+'-'+municipio.c+'">'+departamento+'-'+municipio.c+'</option>');
          };

        };
      });
    }
var countries = [];
function initMap() {
  

  var customControlSearch =  L.Control.extend({

  options: {
    position: 'topright'
  },

  onAdd: function (map) {
    var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
    var input = L.DomUtil.create('input','form-control',container);
    input.id= 'autocomplete';
    input.placeholder="Buscador de Lugares";
    //container.style.width = '30%';
    //container.style.height = '30px';

    loadOption ();  
  
    container.onclick=function  () {
      input.value=""; 
    }
    
    return container;
  }
});


  var customControl =  L.Control.extend({

    options: {
      position: 'topleft'
    },

    onAdd: function (map) {
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
      var boton = L.DomUtil.create('span','btn btn-default ',container);
      var input = L.DomUtil.create('span','glyphicon glyphicon-eye-open',boton);
     /* input.style.width = '30px';
      input.style.height = '30px';*/
      //container.style.backgroundColor = 'white';     
      //container.style.backgroundImage = "url(http://t1.gstatic.com/images?q=tbn:ANd9GcR6FCUMW5bPn8C4PbKak2BJQQsmC-K9-mbYBeFZm1ZM2w2GRy40Ew)";
      //container.style.backgroundSize = "30px 30px";
      //container.style.width = '30px';
      //container.style.height = '30px';

      container.onclick = function(){
        console.log('buttonClicked');
        
        var redirectWindow = window.open('http://maps.google.com/maps?q=&layer=c&cbll='+$("#gasLat").val()+','+$("#gasLon").val()+'&cbp=11,0,0,0,0', '_blank');
        redirectWindow.location;
      }

      return container;
    }
  });

  var customControlMark =  L.Control.extend({

    options: {
      position: 'topleft'
    },

    onAdd: function (map) {
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
      var boton = L.DomUtil.create('span','btn btn-default ',container);
      var input = L.DomUtil.create('span','glyphicon glyphicon-map-marker',boton);
     

      container.onclick = function(){
        console.log('buttonClicked');
        var center = map.getCenter();
        console.info(center);
        eventDragEnd(center.lat,center.lng);
        //console.info(map);
        if (map) {
          setTimeout(function () {
            map.panTo([center.lat,center.lng]);
          },500)
          

        } 

        
      }

      return container;
    }
  });


  map = new LMap([5,-74],6);
  map.map.addControl(new customControl());
  map.map.addControl(new customControlSearch());
  map.map.addControl(new customControlMark());
  $('#autocomplete').autocomplete({
      lookup: countries,
      onSelect: function (suggestion) {
        console.info(suggestion);
        map.map.panTo([suggestion.y,suggestion.x]);
        map.map.setZoom(12);
          
      }
  });
}

function addDragable(marker,evento) {
  //console.info("addDragable",marker);

  if (marker) {

    marker.marker.dragging.enable();
    marker.marker.on('dragend', evento);
  }

  

}

function eventFormCoordChange(lat,lon){

  if (marker) {
    lat = lat || 0;
    lon = lon || 0;

    marker.setPosition(lat,lon)
    map.setCenter([lat,lon]);
    map.setZoom(15);

  }
}

function diffObject(obj1, obj2) {
  var result = {};
  for (key in obj1) {
    if (obj2[key] != obj1[key]) result[key] = obj2[key];
    if (typeof obj2[key] == 'array' && typeof obj1[key] == 'array')
      result[key] = arguments.callee(obj1[key], obj2[key]);
    if (typeof obj2[key] == 'object' && typeof obj1[key] == 'object')
      result[key] = arguments.callee(obj1[key], obj2[key]);
  }
  return result;
}

function eventDragEnd(lat,lon){
  $("#gasLat").val(lat).trigger('input');
  $("#gasLon").val(lon).trigger('input');
}

function createMarker(lat,lon){
  marker = new Marker(lat,lon);
  marker.add(map);
  addDragable(marker,function(event){
    var marker = event.target;
    var position = marker.getLatLng();

    eventDragEnd(position.lat,position.lng);

  });
  
}

var app = angular.module('gasmarket',
  [
    'datetimepicker'
  ])
  .config([
    'datetimepickerProvider',
    function (datetimepickerProvider) {
      datetimepickerProvider.setOptions({
        viewMode: 'years',
        locale: 'es',
        format: 'L'
      });
    }
  ]);
app.controller('editController', ['$scope', function ($scope){
  //$scope.objEDS = {"cat":null,"nom":null,"cit":null,"dep":null,"mar":null,"lat":null,"lon":null,"dir":null,"id":2,"was":0,"rcar":0,"oil":0,"ins":0,"hot":0,"res":0,"gro":0,"smon":0,"atm":0,"gym":0,"ffo":0,"scel":0,"trep":0,"par":0,"obs":"","cer":"","rvp":"","tel":"","vol":0,"vcon":"","prv1":"","prv2":"","eml":"","rpl":"","loc":"loc","mrkid":"mrk_2_"};


  $scope.objEDS = {};

  $scope.objEdsOrig = {};

  $scope.vm = {
    datetime: ''
  };
  $scope.$watch('vm', function(actual) {

    console.info("watch actual",actual);
    //$scope.objEDS.vcon = actual.datetime;
    actual.datetime = (actual.datetime!=="Invalid date")?actual.datetime:null;
    actual.datetime = actual.datetime || "01/01/1970";
    $scope.objEDS.vcon = moment(actual.datetime,'DD/MM/YYYY').format('YYYY/MM/DD');
  },true);


  $scope.$watch("objEDS.lat+objEDS.lon", function (actual) {
    var lat = $scope.objEDS.lat;
    var lon = $scope.objEDS.lon;

    eventFormCoordChange(lat,lon);
  },true);

  /*$scope.$watch('objEDS.lat', function(actual) {

    console.info("watch actual",actual);
//eml,lat, lon,obs,rvp

  },true);*/

  /*$scope.vcont = {};
   $scope.$watch('vcont', function(actual) {

   console.info("watch actual",actual);
   //tempActual = actual;
   if (actual) {
   //$scope.objEDS.vcon = actual.yyyymmdd();
   }
   });*/

  $scope.actualizaObjeto = function (){

    console.info("actulizando",$scope.objEdsOrig);
    console.info("actualizado",$scope.objEDS);
    var despues = diffObject($scope.objEdsOrig, $scope.objEDS);
    var antes = diffObject($scope.objEDS, $scope.objEdsOrig);

    var modificaciones = {
      antes: antes,
      despues: despues
    };

    console.info("diferencia",modificaciones );
    //console.info("fecha nueva",$scope.objEDS.vcon);
    //console.info("loc",$scope.objEDS.lat,$scope.objEDS.loc);
    $scope.objEDS.loc = ($scope.objEDS.lat)? "loc" : null;
    //console.info("loc",$scope.objEDS.loc);
    //$("#objEDS").html(""+JSON.stringify($scope.objEDS));
    $scope.objEDS.modificaciones = JSON.stringify(modificaciones);
    //$scope.objEDS.vcon = $scope.objEDS.vcom || "01-01-1970";
    var succes = function (res) {
      if(res.d)
      {

        MdlStorage.sendMsg("updateObjDetail",$scope.objEDS);
        $scope.objEdsOrig = $scope.objEDS;
        //TODO mostrar mensaje que se actualizo correctamente
        delete $scope.objEDS.modificaciones;
        //alert(res.d);
        $.toaster({ message : 'Los datos se han guardado con exito', title : 'Proceso Exitoso', priority : 'success',settings :{timeout:6000} });
      }
    };
    var obj = $scope.objEDS;
    console.info("obj a enviar",obj);
    $.ajax({
      type: "POST",
      dataType: "json",
      contentType: "application/json; chartset:utf-8",
      //url: "data/combustible_point5_2.json",
      //url: "gasmarket.json",
      url: "GasFillDep.aspx/updaten",
      data: JSON.stringify(obj),
      success: succes,
      error: function (error) {
        console.error(error);
        //alert(false);
        $.toaster({ message : 'No se pudo guardar', title : 'Error', priority : 'danger',settings :{timeout:6000} });

      },
      async: true
    });


  };


  var indexCallback= {
    sendObjDetail: function (value) {
      if(value){
        console.info("cargo la pagina",value);
        if(value.msg)
        {
          //cargo el objeto y lo pinto
          $scope.$apply(function () {
            $scope.objEDS = value.msg;
            $scope.vm.datetime =  moment($scope.objEDS.vcon, "MM/DD/YYYY").format('DD/MM/YYYY');

          console.info("$scope.objEDS.vcon",$scope.objEDS.vcon);
            console.info("$scope.vm.datetime",$scope.vm.datetime);
            console.info('moment($scope.objEDS.vcon, "YYYY/MM/DD").format("DD/MM/YYYY")',moment($scope.objEDS.vcon, "YYYY/MM/DD").format('DD/MM/YYYY'));
            $scope.objEdsOrig = $.extend(true,{},value.msg);

            console.info("pintanto objeto",value.msg,$scope.objEDS);
            $("#objEDS").html(""+JSON.stringify($scope.objEDS));

            angular.element("#cargando").hide(1000);
            initMap();
            createMarker(value.msg.lat,value.msg.lon);
          });

        }
      }
    }
  };


  angular.element(document).ready(function () {
    console.info("ready");
    /*$("[type=date]").datetimepicker({
     viewMode: 'years',
     locale: 'es',
     format: 'LL'
     });
     */

    MdlStorage.ini();
    MdlStorage.idem="datail";
    MdlStorage.indexCallback=indexCallback;
    MdlStorage.sendMsg("endLoad",true);


  });

}]);


