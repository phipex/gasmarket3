/**
 * Created by phipex on 21/01/2016.
 */
var BootstrapUtil={
  getListClass:function(selector){
    var classList = $(selector).attr("class");
    return classList.split(/\s+/);
    /*
     var element = document.querySelector(selector);

     if (element && element.className) {
     var classList = element.className.split(/\s+/);
     return classList;
     }*/
  },
  getObjetColClass:function(selector){
    var res = {};
    var listClas = this.getListClass(selector);
    //console.info(listClas);
    if (listClas) {
      for (var i = 0; i < listClas.length; i++) {
        var clas = listClas[i];
        var list = clas.split("-");
        console.info(list);
        if(list[0] === "col"){
          res[list[1]]=list[2];
        }
      }
    }
    
    return res;
  }
};

(function ($) {
  var getVisibleBrother=function(selector){
    var $dom = $(selector);
    //console.info($dom);
    var $parent = $dom.parent();
    //console.info($parent);
    var $list = $parent.children(); //$("#uno").parent().children().not(".col-md-6")
    //console.info($list);
    var $visible = $list.not(selector).not(".evershow").not(".col-0");
    return $visible;


  };

  var getListClass=function(selector){
    var classList = $(selector).attr("class");
    if(classList){
      return classList.split(/\s+/) || [];
    }

    /*
     var element = document.querySelector(selector);

     if (element && element.className) {
     var classList = element.className.split(/\s+/);
     return classList;
     }*/
  };
  var getObjetColClass=function(selector){
    var res = {};
    var listClas = getListClass(selector);
    //console.info(listClas);
    for (var i = 0; i < listClas.length; i++) {
      var clas = listClas[i];
      var list = clas.split("-");
      console.info(list);
      if(list[0] === "col"){
        res[list[1]]=list[2];
      }
    }
    return res;
  };

  var showCol=function($_this){
    console.log("showCol",$_this);
    $_this.removeClass("col-0");//retira la clase que oculta
    var objColThis = getObjetColClass($_this);//retorn el objeto con las clases de columna
    
    var $brother = $(".evershow");//selecciona el objeto que debe ser siempre mostrado
    var objColBrother = getObjetColClass($brother);//retorn el objeto con las clases de columna
    //var objOldColBrother = $brother.data("objcol");
    console.info("objColBrother,objColThis",objColBrother,objColThis);
    var keys = Object.keys(objColThis);

    for (var i = 0; i < keys.length; i++) {//recorre todas las clases para intercambiar los valores de la columna
      var key = keys[i];
      var valOld = parseInt(objColBrother[key]);
      
      if (valOld) {
        var valAdd = parseInt(objColThis[key]);
        console.info("col-" + key + "-"+valOld, "col-" + key + "-"+(valOld-valAdd));
        $brother.removeClass("col-" + key + "-"+valOld);
        $brother.addClass("col-" + key + "-"+(valOld-valAdd));
      }
    }
  };

  var hideCol=function ($_this) {

    var objColThis = getObjetColClass($_this);
    var $brother = $(".evershow");
    var objColBrother = BootstrapUtil.getObjetColClass($brother);
    var keys = Object.keys(objColThis);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var valOld = parseInt(objColBrother[key]);
      if (valOld) {
        var valAdd = parseInt(objColThis[key]);
        console.info("col-" + key + "-"+valOld, "col-" + key + "-"+(valAdd+valOld));
        $brother.toggleClass("col-" + key + "-"+valOld);
        $brother.addClass("col-" + key + "-"+(valAdd+valOld));
      }
    }
    $($_this).addClass("col-0");
    //$brother.data("objcol",objColBrother);

  };

  function hideBrother ($_this) {
    var $visibleBrother = getVisibleBrother($_this);//retorna los hermanos visibles
    console.info("intercambiarColumna $visibleBrother",$visibleBrother);
    if ($visibleBrother.length !== 0) {//oculta el hermno visible
      hideCol($visibleBrother);//TODO cambiar para tenga mas columnas
    }
  }

  function intercambiarColumna ($_this) {

    hideBrother ($_this);
    showCol($_this);//muestra la columna actual
  }


  $.fn.extend({

    toggleCol:function(action){
      var $_this = $(this);//retorna el elemento seleccionado
      

      if (!$_this.hasClass("evershow")) {//si es el elemnto principal no hace nada (por que es un error)
        var isHide = $_this.hasClass("col-0");
        if (action) {
          console.info("se ha enviado un accion");
          
          if (action === "show" && isHide) {
            console.log("la accion es mostrar");
            intercambiarColumna ($_this);
          } else if(action === "hide" && !isHide){
            console.log("la accion es ocultar");
            hideCol($_this);//oculta la columna actual
          };
          return;
        }
        
        if (isHide) {//verifica si esta oculto
          //console.log("esta escondido");
          intercambiarColumna ($_this);
        } else {
          //console.log("no esta escondido");
          hideCol($_this);//oculta la columna actual
        }
      }
    }

  });
  $(document).on("click","[data-togglecol]",function(){

    var id = $(this).data("togglecol");
    console.log("click para mostrar",id);
    $(id).toggleCol();
  });
})(jQuery);
